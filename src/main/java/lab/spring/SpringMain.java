package lab.spring;

import lab.spring.config.TravelConfig;
import lab.spring.impl.Hotel;
import lab.spring.impl.Plane;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDate;

public class SpringMain {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(TravelConfig.class);
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        Person kowalski = new Person("Jan", "Kowalski");
        kowalski.setTicket(new Voucher(LocalDate.now().minusDays(1)));

        Travel t = context.getBean(Travel.class);
                //(Travel)context.getBean("travel");

        t.travel(kowalski);

        Travel t2 = context.getBean(Travel.class);

        System.out.println("done.");
    }
}
