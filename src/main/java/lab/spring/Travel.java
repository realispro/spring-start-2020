package lab.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class Travel {

    //@Autowired
    private String name;

    @Autowired
    @Air
    private Transportation transportation;

    @Autowired
    private Accomodation accomodation;

    public Travel() {
        System.out.println("constructing trip");
    }


    public Travel(Transportation transportation, Accomodation accomodation, String name) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        System.out.println("constructing trip with parametrized constructor...");
    }

    public void travel(Person p){
        System.out.println("started travel for a person " + p);
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }


   /* public void setTransportation(Transportation transportation) {
        System.out.println("setting transportation");
        this.transportation = transportation;
    }


    public void setAccomodation(Accomodation accomodation) {
        this.accomodation = accomodation;
    }
*/
    public String getName() {
        return name;
    }


   /* public void setName(String name) {
        this.name = name;
    }
*/
    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
