package lab.spring;

import java.time.LocalDate;

public class Voucher {

    private LocalDate validFor;

    public Voucher(LocalDate validFor) {
        this.validFor = validFor;
    }

    public LocalDate getValidFor() {
        return validFor;
    }
}
