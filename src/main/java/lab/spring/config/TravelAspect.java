package lab.spring.config;

import lab.spring.Person;
import lab.spring.Voucher;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Aspect
public class TravelAspect {

    @Pointcut("execution(public * *(..))")
    void everythingPointcut(){}


    @Before("everythingPointcut()")
    void logEntering(JoinPoint jp){
        System.out.println("[entering] " + jp.toShortString() + ", " + jp.getTarget().getClass().getSimpleName());

    }

    @After("everythingPointcut()")
    void logExiting(JoinPoint jp){
        System.out.println("[exiting] " + jp.toShortString());
    }

    @Before("execution(public * *(lab.spring.Person))")
    void validateTicket(JoinPoint jp){
        Person person = (Person)jp.getArgs()[0];
        if(person.getTicket().getValidFor().isBefore(LocalDate.now())){
            //throw new RuntimeException
            System.out.println("[validation] ticket not valid. Detected at: " + jp.toShortString());
        }
    }

    @Around("execution(public * travel(lab.spring.Person))")
    Object doSomething(ProceedingJoinPoint jp) throws Throwable {

        Object o = null;
        // Before
        Person person = (Person)jp.getArgs()[0];
        if(person.getTicket().getValidFor().isBefore(LocalDate.now())){
            System.out.println("[replacing] ticket not valid. Detected at: " + jp.toShortString());
            person.setTicket(new Voucher(LocalDate.now().plusDays(1)));
        }
        try {
            o = jp.proceed(jp.getArgs());
        } catch (Throwable t) {
            return null;
        }
        //After

        return o;
    }

}
