package lab.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan("lab.spring")
@EnableAspectJAutoProxy
@PropertySource("classpath:meal.properties")
public class TravelConfig {

    @Autowired
    Environment env;

    @Value("${meal.name}")
    private String custom;

    @Bean("name")
    public String name(){
        return new String("name from method");
    }

    @Bean
    public List<String> meal(){
        //String custom = env.getProperty("meal.name");
        return Arrays.asList("ziemniaczki", "schabowy", "buraczki", custom);
    }

}
