package lab.spring.impl;

import lab.spring.Accomodation;
import lab.spring.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component("accomodation")
public class Hotel implements Accomodation {

    @Resource
    @Qualifier("meal")
    private List<String> meals;

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }
}
