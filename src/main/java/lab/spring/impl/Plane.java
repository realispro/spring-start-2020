package lab.spring.impl;

import lab.spring.Air;
import lab.spring.Person;
import lab.spring.Transportation;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("plane")
@Lazy
@Scope("prototype")
@Air
public class Plane implements Transportation, InitializingBean {

    @Override
    public void transport(Person p) {
        System.out.println("Person " + p + " is being transported by plane");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("plane: after properties set");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("plane: post construct");
    }

}
